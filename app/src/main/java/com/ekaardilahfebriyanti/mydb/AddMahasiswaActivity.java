package com.ekaardilahfebriyanti.mydb;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddMahasiswaActivity extends AppCompatActivity {
    EditText nrp,nama,prodi;
    Button insert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mahasiswa);

        nrp = (EditText) findViewById(R.id.nrpMhs);
        nama = (EditText) findViewById(R.id.namaMhs);
        prodi = (EditText) findViewById(R.id.prodi);
        insert = (Button) findViewById(R.id.btnInsert);
        insert.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                String sNrp = nrp.getText().toString().trim();
                String sNama = nama.getText().toString().trim();
                String sProdi = prodi.getText().toString().trim();
                if(!sNama.isEmpty() && !sNrp.isEmpty() && !sProdi.isEmpty()){
                    if(sNrp.length()==9){
                        if(sNama.length()>=3){
                            if(sNama.matches("[a-zA-Z]+$")){
                                DatabaseHelper dbHelper = new DatabaseHelper(AddMahasiswaActivity.this);
                                SQLiteDatabase db = dbHelper.getWritableDatabase();
                                dbHelper.createMahasiswaTable(db);
                                dbHelper.insertDataMahasiswa(db, sNrp, sNama, sProdi);
                                Toast.makeText(AddMahasiswaActivity.this, "Insert Data Mahasiswa Success", Toast.LENGTH_LONG).show();
                                nrp.setText("");
                                nama.setText("");
                                prodi.setText("");
                            }else {
                                Toast.makeText(AddMahasiswaActivity.this, "nama harus berisi huruf", Toast.LENGTH_SHORT).show();
                            }

                        }else {
                            Toast.makeText(AddMahasiswaActivity.this, "nama minimal 3 huruf", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        Toast.makeText(AddMahasiswaActivity.this, "NRP harus 9", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(AddMahasiswaActivity.this, "data harus diisi lengkap", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
